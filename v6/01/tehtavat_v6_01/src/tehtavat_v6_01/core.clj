(ns tehtavat-v6-01.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  
  
  
  ;TEHT 1
  (println "TEHT 1 ")
  
  (def vuosi1 [-3, -5, -2, 5, 8, 15, 17.5, 16, 15, 7.5, 2, -3])
  (def vuosi2 [-4, -5, -2, 4, 10, 14, 17, 16, 12, 8, 2, -2])
  
  (def keskiarvot
    (map #(/ % 2)
    (map + vuosi1 vuosi2)
    )
  )
  
  (def positiiviset 
    (filter #(> % 0) keskiarvot)
  )
  
  ;siivilöi taulukosta positiiviset kk keskiarvot ja laske keskilämpötila
  (println(format "%.1f"(double(/ (reduce + positiiviset) (count positiiviset)))))


  ;TEHT 2
  (newline)
  (println "TEHT 2 ")
  
  (def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])
 
  (def huhtikuu
    (filter #(> (:kk %)3) food-journal)
  )

  (def neste
    (apply + (map :neste huhtikuu))
    )
  
  (def vesi
    (apply + (map :vesi huhtikuu))
    )

  ;20,6-10,3 =10,3
  (println (format "%.1f"(- neste vesi)) )
 
 
 ;TEHT 3
  (newline)
  (println "TEHT 3 ")
  
  (def muuneste
    (map #(assoc % :muuneste (format "%.1f"( - (% :neste) (% :vesi))))huhtikuu)
    )
  
  
  (def poistaNesteet
    ;(map #(select-keys % [:kk :paiva] )huhtikuu)
    (map #(apply dissoc % [:neste :vesi]) muuneste)
    
  )
    (println poistaNesteet)
    
  ;MAIN ENDS
  )
