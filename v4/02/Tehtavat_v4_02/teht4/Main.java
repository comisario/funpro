/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht4;

import java.util.*;
import java.util.stream.Stream;

/**
 *
 * @author neaval
 */
public class Main {
     public static void main(String[] args) {
        Omena kuulas= new Omena("Valkea kuulas");
        Omena raike = new Omena("Raike");
        
        Stream<Omena> omenaStream = Stream.of(kuulas, raike);
    
        List<Omena> omenaLista = omenaStream.collect(
            //luodaan uusi ArrayList    
            ArrayList::new,
            //ArrayListaan lisätään halutut elementit (tässä kuulas ja raike)    
            List::add,
            //Appends all of the elements in the specified collection to the end of this list, in the order that they are returned by the specified collection's Iterator.
            List::addAll);
        
        System.out.println(Arrays.toString(omenaLista.toArray()));
        
        //[Omena lajike: Valkea kuulas, Omena lajike: Raike]
}
}