function fact(n) {
// triviaalitapaus
  if (n === 0) {
    return 1;
  }
 // perussilmukka
  return n * fact(n - 1);
}
var tulos = fact(4);
console.log(tulos);



//tehtävä 1
function onPalindromi(merkkijono) {
  if(merkkijono.length == 0 || merkkijono.length == 1){
    return true;
  }else if(merkkijono.charAt(0)!== merkkijono.slice(-1)){
    return false;
  }else{
    var uusiMerkkijono = merkkijono.slice(1, -1);
    return onPalindromi(uusiMerkkijono);
  }
}
console.log("\nTeht 1");
var palindromi = onPalindromi("sokos");//true
console.log(palindromi);
var palindromi2 = onPalindromi("Spacepirates");//false
console.log(palindromi2);


//tehtävä 2
function syt(p, q){
  if(q == 0){
    return p;
  }else{
    return syt(q, p%q);
  }
}

var suurYhtTekija = syt(102, 68);
console.log("\nTeht 2");
console.log(suurYhtTekija);


//tehtävä 3
function kjl(p, q){
  if(q == 1){
    return true;
  }else if(q == 0){
    return false;
  }else{
    return kjl(q, p%q);
  }
}

console.log("\nTeht 3");
var jaoton = kjl(35, 18);//true
console.log(jaoton);
var jaoton2 = kjl(10, 5);//false
console.log(jaoton2);



//tehtävä 4
function potenssiin(luku, exp){
  if (exp == 0){
    return 1;
  }else{
    return luku * potenssiin(luku, exp - 1);
  }
}
console.log("\nTeht 4");
var korotus = potenssiin(2, 4);//true
console.log(korotus);
var korotus2 = potenssiin(4, 0);// palauttaa 1
console.log(korotus2);



//tehtävä 5
function kaannaLista(lista){
  var reversed = [];
  
  function kaantaja(lista){
    if(lista.length !== 0){
      reversed.push(lista.pop());
      kaantaja(lista);
    } 
    
  }
  kaantaja(lista);
  return reversed;
}

var kaannetty = kaannaLista([1, 2, 3, 4]);
console.log("\nTeht 5");
console.log(kaannetty);