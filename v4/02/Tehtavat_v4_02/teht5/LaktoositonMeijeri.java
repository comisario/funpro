/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht5;

/**
 *
 * @author valto
 */
public class LaktoositonMeijeri extends AbstractFactoryMeijeri {
    @Override
    LaktoositonMaito getMaito() {
        return new LaktoositonMaito();
    }

    @Override
    LaktoositonJuusto getJuusto() {
        return new LaktoositonJuusto();
    }

    @Override
    VahalaktoositonJugurtti getJugurtti() {
       return new VahalaktoositonJugurtti();
    }
    
}
