
//TEHT 1
let toCelsius = fahrenheit => (5/9)*(fahrenheit -32);
let area = radius => Math.PI * radius * radius;

console.log("TEHT 1")
console.log(toCelsius(100));
console.log(area(2));


//TEHT 2
var movies = require('./movies.js');
var titleRelease = movies.movies.map(movie => ({title: movie.title, release: movie.release }));

console.log("\nTEHT 2");
console.log(titleRelease);


//TEHT 3
var filtered = movies.movies.filter(movie => movie.release > 2011);
console.log("\nTEHT 3");
console.log(filtered);


//TEHT 4
var vuosi1 = [-3, -5, -2, 5, 8, 15, 17.5, 16, 15, 7.5, 2, -3];
var vuosi2 = [-4, -5, -2, 4, 10, 14, 17, 16, 12, 8, 2, -2];
var vuodet = [];

//vuosien 2015 ja 2016 keskiarvot
for (var i = 0; i < vuosi1.length; i++) {
    vuodet.push((vuosi1[i] + vuosi2[i]) / 2);
}

//taulukosta poimitaan positiiviset kuukausikeskiarvot
var posVuodet = vuodet.filter(kuukausi => kuukausi > 0);
//positiivisista keskiarvoista lasketaan keskiarvo
var posKeskiarvo = posVuodet.reduce((previousValue, currentValue) => (previousValue + currentValue)) / posVuodet.length;

console.log("\nTEHT 4");
console.log(posKeskiarvo);


//TEHT 5
var fs = require("fs");
var kalevala = fs.readFileSync("./kalevala.txt", "utf-8").toLowerCase();

let sanat = kalevala.match(/[^,.:; \r\n][\wäöåÄÖÅ']*/g);
let esiintymalista = [];

sanat.map(sana => {var kappaleLKM = sanat.reduce(
   function(total, x){return sana==x ? total+1 : total}, 0);
    esiintymalista.push(sana + " : " + kappaleLKM);
});

console.log("\nTEHT 5");
console.log(esiintymalista);


