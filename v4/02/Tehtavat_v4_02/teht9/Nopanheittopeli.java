/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht9;

import java.util.*;

/**
 *
 * @author neaval
 */
public class Nopanheittopeli extends Game{
    private int[] silmaluku = null;
    
    private Random noppa = null;
    private int voittaja = -1;
    
    @Override
    void initializeGame() {
     // Initialize players
        System.out.println("**Voittaja on se joka saa ensin heitettyä noppien summaksi 15**\n");
         silmaluku = new int[playersCount];
        for (int i = 0; i < playersCount; i++) {
           silmaluku[i] = 0;
        }

        noppa = new Random();

        voittaja = -1;    
    }

    @Override
    void makePlay(int player) {
    // Roll the die
    int nopanheitto = noppa.nextInt(6) + 1;
        //luku.add(nopanheitto);        

        silmaluku[player] += nopanheitto;
        System.out.println("Pelaaja " + (player + 1) + " heitti numeron " + nopanheitto );
        System.out.println("summaksi saatiin = "+ silmaluku[player]+"\n");
        
        
        if (silmaluku[player] > 15) {
           voittaja = player;
        }
    }

    @Override
    boolean endOfGame() {
        return (voittaja != -1);
    }

    @Override
    void printWinner() {
        System.out.println("Pelaaja #" + (voittaja +1) + " voitti pelin!");
    }
   
    
}
