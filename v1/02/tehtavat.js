'use strict'

//TEHT 1

const vertaaLukuja = function () {
    return function (luku1, luku2) {
		if(luku1 > luku2){
		    return 1;
		}else if(luku2 > luku1){
		    return -1;
		}else{
		    return 0;
		}
	 };
	}(); 

console.log("TEHT 1");
let lukujenVertaus = vertaaLukuja(4, 2);
console.log(lukujenVertaus);
lukujenVertaus = vertaaLukuja(1, 2);
console.log(lukujenVertaus);


//TEHT 2
function vertaaKeskilampotilaa(vertaa, vuosi1, vuosi2){
    let counter = 0;
    for(var i =0; i < vuosi1.length; i++){
        if(vertaa(vuosi1[i], vuosi2[i]) === -1){
            counter++;
        }
    }
   return counter;
}

let lista1 = [-3, -5, -2, 5, 8, 15, 17.5, 16, 15, 7.5, 2, -3];
let lista2 = [-4, -5, -2, 4, 10, 14, 17, 16, 12, 8, 2, -2];// 3 pitäsi olla suurempi kuin vuonna 1

let vertaus = vertaaKeskilampotilaa(vertaaLukuja, lista1, lista2);
console.log("\nTEHT 2");
console.log(vertaus);



//TEHT3
const offset = [1,2];
const zoom = 2;

const point = { x: 1, y: 1};

const pipeline  = [   // 2D-muunnoksia
    
    function translate(p){
        return { x: p.x + offset[0], y: p.y + offset[1] };
    },

    function scale(p){
        return { x: p.x * zoom, y: p.y * zoom};
    },
    
    function rotation(p){
        return {
            x:p.x * Math.cos(Math.PI) -p.y *Math.sin(Math.PI).toFixed(0), 
            y: p.y *Math.cos(Math.PI)+ p.x*Math.sin(Math.PI).toFixed(0)};
    }
];


function muunnos(point){
     for(let i=0; i<pipeline.length; i++){   
        point = pipeline[i](point);
    }
    return point;
}

console.log("\nTEHT 3");
console.log(point);
console.log(muunnos(point));// (1,1)->(-4,-6)



//TEHT 4 -Tehty ensimmäisessä häntärekursion avulla
function potenssiin(luku, exp){
  if (exp == 0){
    return 1;
  }else{
    return luku * potenssiin(luku, exp - 1);
  }
}
console.log("\nTEHT 4");
var korotus = potenssiin(2, 4);//true
console.log(korotus);



//TEHT 5

let f, g;
function foo() {
  let x;
  f = function() { return ++x; };
  g = function() { return --x; };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  
console.log('call to g(): ' + g()); 
console.log('call to f(): ' + f()); 

/* Funktiota foo kutsuttaessa luodaan pinoon muuttuja x.  Tämän jälkeen funktiot
    f ja g määritellään ja x kopioidaan kasaan liitetään funktioiden sulkeumiin.
    
    Tämän jälkeen kutsutaan funktiota f, jolloin x:n arvo kasvaa yhdellä, eli x:n arvo on tällöin 2.
    Kun funktio foo päättyy, poistuu x pinosta, mutta muuttujan kopio säilyy kasassa (funktioiden f ja g sulkeumissa).
    
    Kun kutsutaan funktiota g, sen sulkeumaan liitetty muuttuja x pienenee yhdellä, ja kun kutsutaan funktiota f
    kasvaa sen sulkeumaan liitetty muuttuja (sama muuttuja kuin funktiossa g) yhdellä.
    
    Tietoiskuissa ei käsitelty sitä, että muuttujan arvo voi olla liitettynä funktioon g että f sulkeumiin samanaikaisesti
*/
    
    
    
    
    
    
    