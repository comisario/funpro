//TEHT 1

function laskePisteet(pituus, kPiste, lisapisteet){
    let pisteet = 0;
    
    if(pituus >= kPiste){
        pisteet = 60;
        
        if(pituus > kPiste){
            let lisametri = pituus - kPiste;
            pisteet = pisteet + (lisametri * lisapisteet);
        }
    }else{
        let alimetri = kPiste - pituus;
        pisteet = 60 -(alimetri * lisapisteet);
    }
    return  pisteet;
}

function pisteidenLaskija(kPiste, lisapisteet){
    return pituus => laskePisteet(pituus, kPiste, lisapisteet);
}

const normaalimaki = pisteidenLaskija(85, 2);
const suurmaki = pisteidenLaskija(120, 1.8);

let pisteet1 = normaalimaki(90); //70p
let pisteet2 = normaalimaki(60); //10p
let pisteet3 = suurmaki(130);    //78p

console.log("TEHT1");

console.log(pisteet1);
console.log(pisteet2);
console.log(pisteet3);


//TEHT2
const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    
    class Auto{
        constructor(){
            this.tankki = 0;
            suojatut.set(this, {matkamittari: 0});
        }  // suojatut instanssimuuttujat nimi ja ika
        
     
        getTankki() {return this.tankki;}
        getMatkamittari() {return suojatut.get(this).matkamittari} // testausta varten
        
        setTankki(polttoaine) {this.tankki += polttoaine;}  
        
        aja(){
            if (this.tankki > 0) {
                console.log("\nAjan");
                this.tankki--;
                suojatut.get(this).matkamittari++;
            } else {
                console.log("\nBensa loppu");
            }
        }
        
    }
    
    return Auto;
})();

console.log("\nTEHT2");

const mersu = new Auto();

mersu.setTankki(2);
console.log("Polttoaine: " + mersu.getTankki());
console.log("Matkamittari: " + mersu.getMatkamittari());

for (var i = 0; i < 3; i++) {
    mersu.aja();
    console.log("Polttoaine: " + mersu.getTankki());
    console.log("Matkamittari: " + mersu.getMatkamittari());
}


//TEHT 3
console.log("\nTEHT3");

const Immutable = require('immutable');
const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = set1 & 'ruskea';

console.log(set1 === set2); //epätosi, ruskeaa ei lisätty settiin 1

const set3 = set2 & 'ruskea';
console.log(set2 === set3); //tosi, koska dublikaatteja ei lisätä

//TEHT 4
console.log("\nTEHT4");

//lazy evaluation lataa vain tarpeellisen kohden listasta, normaali lataa kaiken
//kutsun yhteydessä, ja tulostaa lopuksi halutun listan osan.

let musiikki = ["Sabaton", "Bon Jovi", "Antti Tuisku", "Bile musaa"]
            .map(function(x) {console.log(`Soitetaan seuraavaksi: ${x}`); return "Ja nyt "+ x});

console.log(musiikki[3]);

let musiikki2 = Immutable.Seq(["Sabaton", "Bon Jovi", "Antti Tuisku", "Bile musaa"])
            .map(function(x) {console.log(`\nSoitetaan seuraavaksi: ${x}`); return "Ja nyt "+ x});

console.log(musiikki2.get(3));


//TEHT 5
console.log("\nTEHT5");

//Palauttaa arvon halutun määrän kertoja, jos määrää ei ole määritelty, toistaa se arvoa ikuisesti.
//Määritetään: Immutable.Repeat(arvo, toistokerrat)

var firstTenElements = Immutable.Repeat()
                                .map( Math.random )
                                .take( 10 )
                                .toJSON();

console.log(firstTenElements);

console.log(Immutable.Repeat('foo')); // [ 'foo', 'foo', 'foo', ... ]
console.log(Immutable.Repeat('bar', 4).toJSON()); // [ 'bar', 'bar', 'bar', 'bar' ] 




