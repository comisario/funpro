/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht8;

import java.util.*;

/**
 *
 * @author neaval
 */
public class ObserverTest {
    public static void main(String[] args) {
     
        Subject uutinen = new Subject();
        //Subject s2 = new Subject();
                
        List<String> hesari = new ArrayList<>(); 
        List<String> iltasanomat = new ArrayList<>(); 
        
        uutinen.addObserver((Observable o, Object arg) -> {
            ///*if ((Integer)arg % 2 == 0) */hesari.add((Integer)arg);
             if (arg.equals("politiikka")) hesari.add((String) arg);
        });
        
        uutinen.addObserver((Observable o, Object arg) -> {
            if (arg.equals("julkkis")) iltasanomat.add((String)arg);
        });
        
        Thread t1 = new Thread(uutinen);
        t1.start();
        //Thread t2 = new Thread(s2);
        //t2.start();
           
        try {
            t1.join();
          //  t2.join();
        } catch (InterruptedException ex) {
        }
        
        System.out.println("Hesari: ");
        hesari.forEach (System.out::println);
        System.out.println("\nIlta-Sanomat: ");
        iltasanomat.forEach (System.out::println);
        
    }
    
}
