
public class Testi {
    
     @Test
    public void testmoveAllPointsRightBy() throws Exception {
        List<Point> points = Arrays.asList(new Point(5,5), new Point(10,5));
        List<Point> expectedPoints = Arrays.asList(new Point(15,5), new Point(20,5));
        List<Point> newPoints = Point.moveAllPointsRightBy(points, 10);
        
        int[] expectedPoints = expectedPoints.stream().mapToInt(p->p.getX()).toArray();
        int[] newPoints = newPoints.stream().mapToInt(p->p.getX()).toArray();
        
        assertArrayEquals(expectedPoints, newPoints);
    }

   /* @Test
    public void testmoveAllPointsRightBy() throws Exception {
    List<Point> points = Arrays.asList(new Point(5,5), new Point(10,5));
    
    List<Point> expectedPoints =Arrays.asList(new Point(15,5),new Point(20,5));
    
    List<Point> newPoints = Point.moveAllPointsRightBy(points, 10);
    
    assertEquals(expectedPoints, newPoints);
    }*/
}
