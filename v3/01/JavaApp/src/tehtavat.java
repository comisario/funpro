/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Comparator.comparing;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.DoubleFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import teht2.Dish;
import teht2.Trader;
import teht2.Transaction;

/**
 *
 * @author neaval
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //TEHT 1
        DoubleFunction toCelsius = fahrenheit -> (5.0/9.0) * (fahrenheit - 32);
        DoubleFunction area = radius -> Math.PI * radius * radius;
        
        System.out.println("TEHT 1");
        System.out.println("100 Fahrenheit to Celsius: " + toCelsius.apply(100));
        System.out.println("Area of 2: " + area.apply(2));
        
        
        //TEHT 2
        System.out.println("\nTEHT 2");
         
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
		
	List<Transaction> transactions = Arrays.asList(
            new Transaction(brian, 2011, 300), 
            new Transaction(raoul, 2012, 1000),
            new Transaction(raoul, 2011, 400),
            new Transaction(mario, 2012, 710),	
            new Transaction(mario, 2012, 700),
            new Transaction(alan, 2012, 950)
        );	
        
        List<Transaction> tr = transactions.stream()
                                               .filter(transaction -> transaction.getYear() > 2011)
                                               .filter(transaction -> transaction.getValue() >= 900)
                                               .collect(toList());
        System.out.println(tr);
        
        
        //tehtävän 2 toinen osuus
        List<Dish> menu = Dish.menu;
        List<Object> types = menu.stream()
                                    .map(dish -> dish.getType())
                                    .collect(toList());
        
        Map<Object, Long> counted = types.stream()
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        
        System.out.println(counted);
        
        
        
        //TEHT 3
        
        System.out.println("\nTEHT 3");
        
        List<Integer> numbers = new ArrayList<>();
        Random rand = new Random();
        int max = 6, min = 1;
        
        for(int i = 0; i < 20; i++){
            int number = rand.nextInt(max - min + 1) + min;
            numbers.add(number);
        }
        
        System.out.println(numbers);
        List<Integer> countSix = numbers.stream().filter(i -> i == 6).collect(Collectors.toList());
        System.out.println(countSix);
        
          Map<Integer, Long> count = countSix.stream()
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        
        System.out.println("laskettu" +count);
        
        /*Toinen mahdollinen ratkaisu
        
        long sixes = Stream.generate(() ->Math.round(Math.random()*100))
                .limit(20)
                .map(n->Math.abs(n))
                .map(n -> n%6 +1)
                .filter(n -> n == 6)
                .count();
                
                System.out.println("laskettu kutosia =" +sixes);*/
        
        
        //TEHT 4
        
         System.out.println("\nTEHT 4");
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        List<Integer> list2 = new ArrayList<>(Arrays.asList(3, 4));

        System.out.println(list1);
        System.out.println(list2 + "\n");
        
        List<int[]>list3 = list1.stream()
                            .flatMap(i ->list2.stream().map(j -> new int[] {i,j}))
                            .collect(Collectors.toList());

        
        for (int[] i : list3){
            System.out.println(i[0]+","+i[1]);
        }
        
        
    }
}
