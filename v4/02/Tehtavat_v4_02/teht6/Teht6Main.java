/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht6;

import java.util.function.*;

/**
 *
 * @author neaval
 */
public class Teht6Main {
    public static void main(String[] args) {
  
    
        UnaryOperator<String> valilyontiKäsittelija =
                (String p) -> 
                    {   
                        //Ensimmäinen käsittelijä poistaa dokumentista kaikki ylimääräiset välilyönnit.
                        p = p.replaceAll("\\s+", " "); 
                        return p;
                    };
        
        UnaryOperator<String> skandienMuuttaja =
                (String p) -> 
                    {  
                        //Toinen korvaa skandit seuraavasti: ä,å -> a; ö -> o; Ä,Å -> A; Ö -> O.
                        p = p.replace("ä", "a")
                                .replace("ä", "a")                                
                                .replace("ö", "o")
                                .replace("Ä", "A")
                                .replace("Å", "A")
                                .replace("Ö", "O");
                        return p;
                    };
        
        UnaryOperator<String> oikenkirjoittaja =
                (String p) -> 
                    {  
                        //Kolmas tarkistaa oikeinkirjoituksen: kaikki sturct-sanat korvataan struct-sanoilla.
                        p = p.replaceAll("sturct", "struct"); 
                        return p;
                    };
        

        
        Function<String, String> ketju = valilyontiKäsittelija.andThen(skandienMuuttaja).andThen(oikenkirjoittaja); 
        
        String str = "TiesitkÖ että jåulu tulee vauhdilla         ?? -Tiesin koska kalle sanoi eilen sturct";
        
        String joulu = ketju.apply(str);
        
        System.out.println(str);
        System.out.println(joulu);
        
        
        
    }
    
}
