(ns tehtavat-v5-02.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  
  
  ;TEHT1
  
  (println "TEHT 1 ")
  
  (println "Anna luku: ")
    (let [luku(read-line)]
      
     (if (<= (Integer. luku) 0)   
     (println "Luvun pitää olla suurempi kuin 0")
     (if  (= (rem (Integer. luku) 2) 0)
          (println "Luku" luku "on parillinen") 
          (println "Luku" luku "ei ole parillinen")
          )
      )
    )
    
    ;TEHT2
    (newline)
    (println "TEHT 2 ")
    
    (defn read-line-with-prompt [prompt]
      (println prompt)
      (flush)
      (read-line))
    
    
    ;(println "Anna luku: ")
   (loop [luku(read-line-with-prompt "Anna kokonaisluku")]
      
     (if (> (Integer. luku) 0)
     (if  (= (rem (Integer. luku) 2) 0)
          (println "Luku" luku "on parillinen") 
          (println "Luku" luku "ei ole parillinen")
          )
      
    (recur (read-line-with-prompt "Anna kokonaisluku"))
    )
   )
 
    ;TEHT 3
    (newline)
    (println "TEHT 3 ")
    
    ;loopin avulla tehty
    (defn jaolliset-kolmella-moo [ylaraja]
    (loop [luku 1]
          (when (= (rem (Integer. luku) 3) 0)  
              (println luku))
      (if (<= luku ylaraja) 
          
       (recur (inc luku)))))
  
  
  ;dotimesin avulla tehty
  (defn jaolliset-kolmella [ylaraja]
    (dotimes [n ylaraja] ; suoritetaan 5 kertaa
    (when (= (rem (+ n 1) 3) 0)  
              (println (+ n 1)))) ; n saa arvot 0 -> 4
    )
  
  ;Kutsutaan funktioita
  (println "loop")
  (jaolliset-kolmella-moo 9)
  
  (newline)
  (println "dootimes")
  (jaolliset-kolmella 10)
  
  
  ;TEHT 4
  (newline)
  (println "TEHT 4 ")
  
  (defn lottorivi
    [] ;tyhjä parametri
    (loop [lista #{}]
      (if (< (count lista)7)
      (recur (into lista [(+ (rand-int 39) 1)]))
      
      lista ;palauttaa listan 
      )
    )
  )
  
  (println(lottorivi))
    

  ;TEHT 5
  (newline)
  (println "TEHT 5 ")
  
  (defn syt [p, q] 
     (if (zero? q)p
          (recur q (mod p q))))
        
  (println(syt 10 5))

    
;MAIN LOPPUU
)