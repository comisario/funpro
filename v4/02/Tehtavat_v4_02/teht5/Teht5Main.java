/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht5;

/**
 *
 * @author valto
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        AbstractFactoryMeijeri meijeri = new LaktoositonMeijeri();
        //AbstractFactoryMeijeri meijeri = new VahalaktoosinenMeijeri();
        
        meijeri.getMaito().tulosta();
        meijeri.getJuusto().tulosta();
        meijeri.getJugurtti().tulosta();
    }
    
}
