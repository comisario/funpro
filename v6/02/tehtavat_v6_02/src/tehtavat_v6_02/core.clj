(ns tehtavat-v6-02.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  
  ;TEHT 1
  (println "TEHT 1")
  
  (def allekirjoitus (fn [teksti nimi]
      (println teksti nimi))
      )
  
  (def allekirjoitus1 (partial allekirjoitus "Ystävällisin  terveisin,"))
  (def allekirjoitus2 (partial allekirjoitus "Terveisin,"))
  (def allekirjoitus3 (partial allekirjoitus "Med vänlig häslning,"))
  
  (allekirjoitus1 "Nea")
  (allekirjoitus2 "Maria")
  (allekirjoitus3 "Helen")
  
   
  ;TEHT 2
  (newline)
  (println "TEHT 2")
  
  
  ;A Kohta
  (def vektori [[1 2 3][4 5 6][7 8 9]])
  
  (def minimiarvot (map #(apply min % )vektori))
  (println minimiarvot)
  
  ;B Kohta
  
  (def backToSeq (apply vector minimiarvot))
  (println backToSeq)
  
  
   ;TEHT 3
  (newline)
  (println "TEHT 3")
  
  (def simaresepti 
    [{:aines "Vesi", :yksikko "litraa", :maara 4} 
    {:aines "Sokeri", :yksikko "grammaa", :maara 500} 
    {:aines "Sitruuna", :yksikko "kpl", :maara 2} 
    {:aines "Hiiva", :yksikko "grammaa", :maara 1}]
  )
  
  (defn moninkertainen [resepti luku]
  (map #(update % :maara * luku) resepti)
  )
  
  (run! println (moninkertainen simaresepti 3))
  
  ;MAIN ENDS
  )
