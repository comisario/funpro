(ns tehtavat-v5-01.core
  (:gen-class))

(defn -main [& args] 

;TEHT 2
(+ 4 (* 2 5));TAI
(+ (* 2 5 ) 4) 

(+ 1 2 3 4 5) 

((fn [name] (str "Tervetuloa Tylypahkaan " name)), "Nea")

(get-in{:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}}, [:name :middle])

);END OF MAIN


;TEHT 4
(defn square 
  [x]
  (* x x))

(square 2) ;=> 4
(square 7) ;=> 49

;TEHT 5

(defn karkausvuosi?
    [vuosi]
    
    (cond 
        (= (rem vuosi 400) 0) true  ; TAI (zero? (mod vuosi 400)) true
        (= (rem vuosi 100) 0) false  ;(zero? (mod vuosi 100)) false
        (= (rem vuosi 4) 0) true  ;(zero? (mod vuosi 4)) true
        :default false)

    )
  
  
(karkausvuosi? 100) ;=> false
(karkausvuosi? 200) ;=> false
(karkausvuosi? 400) ;=> true
(karkausvuosi? 12)  ;=> true
(karkausvuosi? 20)  ;=> true
(karkausvuosi? 15)  ;=> false  