/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teht8;

import java.util.*;
import static java.lang.Thread.sleep;

/**
 *
 * @author neaval
 */
public class Subject extends Observable implements Runnable{

    List<String> uutiset = Arrays.asList("julkkis", "politiikka", "lemmikit", "julkkis", "ruoka");
    
    @Override
    public void run() {
        uutiset.forEach((uutinen) -> {
            try {
                setChanged();
                notifyObservers(uutinen);
                sleep(100);
            } catch (InterruptedException ex) {
            }
        });
    }
}
