/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import teht1.Tulostaja;
import java.util.function.Function;
import java.util.function.IntSupplier;

/**
 *
 * @author neaval
 */
public class main {

    private static DecimalFormat df = new DecimalFormat(".##");
    
    public class Tulostaja {
        public void tulosta(Supplier source){
        System.out.println(source.get());
    }

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet) {
        return (pituus) -> {
            double pisteet = 60;
            if (pituus < kPiste) {
                pisteet = pisteet - (kPiste - pituus) * lisapisteet;
            } else if (pituus > kPiste) {
                pisteet = pisteet + (pituus - kPiste) * lisapisteet;
            }
            return pisteet; // puuttuva koodi
        };
    }

    ;
     
    
    public static void main(String[] args) {

        //TEHT 1
        System.out.println("TEHT 1");
        Supplier generatorOldWay = (Supplier) () -> 3;

        Supplier generaattori1 = () -> 2;
        Supplier generaattori2 = () -> (int) (Math.random() * 6 + 1);

        Tulostaja t = new Tulostaja();

        t.tulosta(generatorOldWay);
        t.tulosta(generaattori1);
        t.tulosta(generaattori2);
        t.tulosta(() -> 100);

        //TEHT 2        
        System.out.println("\nTEHT 2");
        DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 2.0);

        System.out.println(normaaliLahti.applyAsDouble(100));

        //TEHT 3
        System.out.println("\nTEHT 3");
        System.out.println("Lambda");

        IntStream stream = IntStream.generate(() -> {
            return 1 + (int) (Math.random() * ((39 - 1) + 1));
        }).limit(7);
        List<Integer> lottovoitto = stream.boxed().collect(Collectors.toList());;

        System.out.println(lottovoitto);

        //Osa 2: Anonyymi 
        //IntStream streem = IntStream(){  
        //void eat(){System.out.println("nice fruits");} 
        
        
        //TEHT 4
        System.out.println("\nTEHT 4");

        IntSupplier fibo = new IntSupplier() {
            private int edellinen = 0;
            private int nykyinen = 1;

            public int getAsInt() {
                int seuraava = this.edellinen + this.nykyinen;
                this.edellinen = this.nykyinen;
                this.nykyinen = seuraava;
                return this.edellinen;
            }
        };
        
        

        IntStream.generate(fibo).limit(10).forEach((n) -> System.out.println(n));

        //TEHT 5
        System.out.println("\nTEHT 5");

        Function siirto = Piste.makeSiirto(1, 2);
        Function skaalaus = Piste.makeSkaalaus(2);
        Function kierto = Piste.makeKierto(Math.PI);
        Function muunnos = siirto.andThen(skaalaus).andThen(kierto);// muodosta tässä yhdistetty funktio

        Piste[] pisteet = {new Piste(1, 1), new Piste(2, 2), new Piste(3, 3)};
        List<Piste> uudetPisteet = new CopyOnWriteArrayList();

        for (Piste p : pisteet) {
            uudetPisteet.add((Piste) muunnos.apply(p));
        }

        //uudetPisteet.forEach(p -> System.out.println(p));
        uudetPisteet.forEach(p -> System.out.println("X: " + df.format(p.getX()) + " Y: " + df.format(p.getY())));



    //Piste Luokka

    public class Piste {
        double x, y;

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public Piste(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Function<Piste, Piste> makeSiirto(int x, int y) {
        return (piste) -> new Piste(piste.getX() + x, piste.getX() + y);
    }

    public static Function<Piste, Piste> makeSkaalaus(int i) {
        return (piste) -> new Piste(piste.getX() * i, piste.getY() * i);
    }

    public static Function<Piste, Piste> makeKierto(double PI) {
         //x:p.x * Math.cos(Math.PI) -p.y *Math.sin(Math.PI).toFixed(0), 
         //y: p.y *Math.cos(Math.PI)+ p.x*Math.sin(Math.PI).toFixed(0)};
         
        return (piste) -> new Piste(piste.getX() * Math.cos(PI) - piste.getY() * Math.sin(PI),
                piste.getY() * Math.cos(PI) + piste.getX() * Math.sin(PI))
;}
    
    }}}
