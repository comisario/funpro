public class Main{
    
        private static class Point{
            
        private static int x;
        private static int y;

        private Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
        
        public static Point moveRightBy(int x) {
            return new Point(Point.x + x, Point.y);
        }
        
        public static List<Point> moveAllPointsRightBy(List<Point> points, int x) {
            return points.stream().map(p -> moveRightBy(x)).collect(toList());
        }
        
    }
 
    public static void main(String[] args) {
        
        List<Point> points = Arrays.asList(new Point(12, 2));
        points.stream().map(p -> p.moveRightBy(1).getX()).forEach(System.out::println);
        
        List<Point> newPoints = Point.moveAllPointsRightBy(points, 10);
        System.out.println(newPoints.get(0).getX());

    }
}

